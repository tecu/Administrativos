# Aprendizaje

1. Conocer y profundizar el conocimiento sobre sub-áreas dentro del área de investigación del Procesamiento del Lenguaje Natural (Preprocesamiento de texto, Extracción de entidades y Clasificación de texto entre otros)

1. Desarrollo de prototipos para el procesamiento automático de texto: Desarrollo de herramientas para brindar soluciones a problemas cotidianos, y como presentar los resultados. 

1. Evaluación de resultados en problemas de Procesamiento de lenguaje natural: ¿cómo saber si mi sistema es bueno o malo?, ¿cómo lo comparo con otros? y ¿cómo determino si un cambio fue mejor peor?

1. Desarrollo de una revisión sistemática de literatura: ¿hay otros que trabajan en temas similares?, ¿que hacen?, ¿cómo leo los artículos científicos?, ¿los leo todxs o solo algunos?, ¿cómo decido?

1. Desarrollo de una metodología de investigación: ¿cómo organizo las pruebas, el software?, ¿la evaluación de resultados en una investigación?, ¿Qué etapas sigo?, ¿cómo preparo los datos?

1. Redacción de artículos científicos: ¿qué partes tiene un artículo científicos?, ¿cómo se redacta?, ¿que contiene cada sección?, ¿cómo acomodo mi trabajo a la estructura del artículo?

1. Presentación de ideas científicas ante audiencias diversas: ¿Cómo les cuento a los demás sobre mi investigación?, ¿cómo lo adapto a diferentes audiencias?, ¿explico lo mismo si son niños, adultos computines o no computines?

1. Trabajar en temas que aportan soluciones al contexto local: Usamos herramientas avanzadas para proponer prototipos con soluciones a problemas que encontramos todos los días.

1. Presentar y evaluar ideas ante pares investigadores: Ante una idea nueva, ¿cuáles son sus beneficios?, ¿sus desventajas?, ¿cómo se podría mejorar la idea?, ¿que recomendaciones mejorarían la idea?


