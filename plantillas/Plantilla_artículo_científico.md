# Plantilla guía para la elaboración de artículos científicos. 

## Secciones generales.

- Título
- Autorxs
- Abstract / Resumen
- Keywords / Palabras clave

## Secciones principales

### Antecedentes

- Descripción del problema desde una perspectiva no técnica. El problema no se
  refiere a la falta de un software, se refiere a que hay ... problema que se
  va a tratar de solucionar creando una herramienta de software para probar que
  tan buenos resultados da.

- ¿Por qué ese problema es importante resolverlo?, ¿a quien beneficia?, ¿quien se ve afectado si se queda a como está?

- ¿Cuales son los beneficios de resolverlo?

- La descripción de como otros han abordado el problema, u otros problemas
  similares, basándose en lo leído en artículos científicos extraídos de
  IEEEXplore, ACM Digital Library, SpringerLink u otras. No en páginas web o la Wikipedia.

- Las referencias a paginas web van como pie de pagina y no aparecen en la bibliografía.


### Descripción de la metodología

- Agregar un diagrama de una vista general del proceso seguido para alcanzar el resultado.

- La descripción de la metodología no debe enfocarse en el software desarrollado ya que este es solo una parte de toda la investigación.

- La metodología debe incluir:

	- Información de los datos utilizados: ¿Qué datos se usaron?, ¿de donde salieron?, ¿por que se usaron esos datos?, ¿que características tienen?
	- Información de las herramientas utilizadas: ¿Cuales herramientas?, ¿por que esas herramientas?, ¿cómo las seleccionó?, ¿cúales fueron los parámetros que usó para configurarlas?
	- Información del prototipo desarrollado: Se puede mencionar o describir de forma poco profunda el software que se desarrollo para los experimentos.
	- Información de cómo se evaluaron los resultados: ¿Qué métricas se utilizaron?, ¿por qué se utilizaron esas métricas?

**Nota:** Es importante mencionar que el artículo debe centrarse en:

	- El problema a resolver
	- Los datos de entrada
	- Los valores de salida

El software desarrollado es esencial para la investigación, pero no debe ser el centro del artículo.

###  Resultados

- Datos duros resultado de ejecutar los experimentos propuestos: por ejemplo:
  *al ejecutar el proceso utilizando los embeddings internacionales se obtuvo
  un resultado de 0.7 en el documento1 y 0.2 en el documento2*

- No se redacta usando viñetas sino en prosa.

- Tablas de valores de salida y una explicación de las tablas de valores (en caso de que las haya)

- Acá solo se responde a la pregunta: ¿cuáles fueron los resultados del experimento?

### Conclusiones

- En esta sección se explica que significan los número presentados en la sección anterior.

	- ¿Si son muy altos eso que implicaciones tiene?, ¿Si son muy bajos?
	- ¿Cómo se comparan esos resultados con los de otros artículos de los que habló en los antecedentes?

- Además debe agregar apreciaciones de los autores sobre los resultados

	- ¿Le parece que los resultados fueron satisfactorios?
	- ¿Se cumplió con el motivo por el cual inicio esta investigación?
	- ¿Son los resultados de utilidad para algo?

- No se redacta usando viñetas.

- No se redacta respondiendo una pregunta por párrafo, sino una redacción con
  algunos párrafos que cubra las respuestas a esas preguntas. Por ejemplo: *Los
  resultados de esta investigacion permiten describir un mecanismo para
  detectar sesgos en genero con resultados "significativos". Las metricas
  implementados permitieron detectar xxx comportamiento en los documentos
  utilizados aunque carecen de especificidad...* 

### Bibliografía

- No generar la bibliografía a mano, utilizar alguna herramienta como Mendeley o Jabref.

