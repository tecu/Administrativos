# Talleres TECU

1. Introducción a Cuadernos de jupyter con cuadernos de jupyter
1. Expresiones regulares.
1. NLP y sus áreas de investigación.
1. Stopwords y Lematización.

1. Texto estructurado o semi-estructurado(XML, JSON, YAML).

1. Procesamiento básico de textos.
	1. Gramáticas

1. Distancia de edición.
1. Algoritmos de búsqueda de textos (strings).
1. Corrección de errores.

1. Crawling + Manejo de hilos.

1. Modelos de lenguaje.
1. Clasificación de textos.
	1. Matriz de confusión.
	1. Métricas de evaluación de modelos de lenguaje.
	1. Sesgo, sobreajuste, validación cruzada.

1. Máquinas de soporte vectorial.
1. Vectores de palabras (Word embeddings)

