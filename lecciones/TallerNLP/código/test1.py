import pandas as pd
import re

# Importa los datos
datos = pd.read_csv('2018-E-c-Es-dev.txt', sep='\t', encoding='utf-8');

# Muestra algunos datos

## Nombres de las columnas 
# print(datos.columns)

## Imprime los cinco primeros valores de la columna Tweet
# print( datos['Tweet'][0:5])

# Preprocesamiento

## Elimina columnas
datos.pop('anticipation')
datos.pop('disgust')
datos.pop('fear')
datos.pop('joy')
datos.pop('optimism')
datos.pop('pessimism')
#datos.pop('sadness')
datos.pop('trust')
datos.pop('surprise')

## aplica una función de preprocesamiento
from preprocesamiento import menciones

## Sobreescribe la misma columna
datos['Tweet'] = datos['Tweet'].apply(menciones)

print('- - -')
#print(datos.columns)
#print( datos['Tweet'][0:5])

import matplotlib.pyplot as plt  

from sklearn.metrics import accuracy_score
from sklearn.metrics import plot_confusion_matrix

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.svm import OneClassSVM

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer


X_train, X_test, y_train, y_test = train_test_split( datos['Tweet'], datos['anger'], random_state=0)

## Bolsa de palabras BoW (unigramas)
# vectorizer = CountVectorizer()

## Bolsa de palabras (bigramas)
# vectorizer = CountVectorizer(ngram_range=(3, 3))

## Bolsa de palabras (TF-IDF)
# vectorizer = TfidfVectorizer(ngram_range=(2, 2))
vectorizer = TfidfVectorizer()

X_train_vec = vectorizer.fit_transform(X_train)

print(vectorizer.get_feature_names())

# print(pd.DataFrame(X_train_vec.A, columns=vectorizer.get_feature_names()).head().to_string())

# Entrenamiento
import numpy as np

tuned_parameters = [{'kernel': ['rbf'], 'gamma': np.logspace(0, 1, num=10, base =0.001, dtype=float), 'C': np.logspace(0, 10, num=5, base =1.6, dtype=int)},
                    {'kernel': ['linear'], 'C': np.logspace(0, 10, num=5, base =1.6, dtype=int)}]

clf = GridSearchCV( SVC(), tuned_parameters, cv=10, verbose=3, n_jobs=6) 


# SVM con kernel lineal
# clf = SVC(random_state=0)

clf.fit(X_train_vec, y_train)
#print(clf.best_params_) 

# Validación

X_test_vec = vectorizer.transform(X_test)

y_test_pred = clf.predict(X_test_vec)

print(accuracy_score(y_test, y_test_pred))

plot_confusion_matrix(clf, X_test_vec, y_test)  
plt.show()

# ----------------------------------------------------------------------

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE

colors = [float(hash(s) % 256) / 256 for s in y_train]

tsne = TSNE(random_state=0)
X_train_tsne = tsne.fit_transform(X_train_vec)

X_train_tsne=pd.DataFrame(X_train_tsne, columns=['tsne1', 'tsne2'])

plt.scatter(X_train_tsne['tsne1'], X_train_tsne['tsne2'], c=y_train)
plt.show()

