# Funciones de preprocesamiento de texto

from nltk.corpus        import stopwords as stpwrds
from nltk.stem.snowball import SnowballStemmer

from gensim             import utils

import re
import functools
import nltk.tokenize as tokenize

def mayusculas(text: str):
    """
    Pasa todas las letras a minúscula
    """

    return text.lower()

def tildes(texto):
    """
    Elimina los accentos
    """


    return utils.deaccent(texto)

def emoticonos(texto):
    """
    Elimina los emoticonos de una lista
    """
     # remove_emoji
    emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # miscellaneous symbols and pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F900-\U0001F9FF"  # supplemental symbols & pictographs
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002600-\U000026FF"  # miscellaneous symbols 
                           u"\U00002B00-\U00002BFF"  # miscellaneous symbols and arrows
                           u"\U00002700-\U000027BF"  # dingbats
                           u"\U0001F650-\U0001F67F"  # ornamental dingbats
                           u"\U00002190–\U000021FF"  # arrows
                           u"\U000024C2-\U0001F251"
                           "]+", flags = re.UNICODE)
    text = emoji_pattern.sub("", texto)

    return text

def escape( text):

    escape_pattern = re.compile("(\\\\n)|(\\\\\")|(\")")
    result         = escape_pattern.sub(" ", text)

    return result

def hashtag( text):
    """
    Elimina completamente las etiquetas de los textos
    """

    pattern = re.compile("\s*#\w+")
    text    = pattern.sub("", text)

    return text


def lematizacion(text):
    """
    Lematiza el texto usando el algoritmo de snowball
    """

    stemmer = SnowballStemmer("spanish")

    return stemmer.stem(text)


def menciones(text):
    """
    Elimina completamente las menciones a usuarios del texto
    """

    pattern      = re.compile("(([^\w]@)|(\s+@)|(^@))\w+")
    cleaned_text = pattern.sub("", text)

    return cleaned_text

def numeros(text):
    """
    Elimina completamente los números del texto
    """

    tokens = tokenize.word_tokenize(text, language="spanish")
    result = [token for token in tokens if not token.isnumeric()]
    result = " ".join(result)

    return result

def puntuacion(text):
    """
    Elimina signos de puntuación del texto
    """


    

    spanish_punctuation = r"""!¡"#$%&'()*+,-./:;<                = >¿?@[\]^_`{|}~¨´§«»¶\\’“”’"""
    pattern             = re.compile(f"[{spanish_punctuation}]")
    cleaned_text        = pattern.sub(" ", text)

    return cleaned_text

def stopwords(texto):
    """
    Elimina la lista de stopwords del español provistos con nktk
    """

    stop      = set(stpwrds.words('spanish'))
    tokens         = tokenize.word_tokenize(texto, language        = "spanish")
    cleaned_tokens = filter(lambda x : x not in stop, tokens)

    return ' '.join(cleaned_tokens)

def url(text):
    """
    Elimina los urls del texto
    """
    pattern = re.compile(
        "(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})")

    result = pattern.sub("", text)
    return result

