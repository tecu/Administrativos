

import pandas as pd
import re



# Importa los datos
datos = pd.read_csv('2018-E-c-Es-dev.txt', sep='\t', encoding='utf-8');


## Muestra algunos datos

## Nombres de las columnas 
print(datos.columns)

## Imprime las 5 primeras líneas de la tabla
print(datos.head())

## Imprime los cinco primeros valores de la columna Tweet
print( datos['Tweet'][0:5])

# Preprocesamiento

## Elimina columnas
datos.pop('anticipation')
datos.pop('disgust')
datos.pop('fear')
datos.pop('joy')
datos.pop('optimism')
datos.pop('pessimism')
#datos.pop('sadness')
datos.pop('trust')
datos.pop('surprise')


## aplica una función de preprocesamiento
from preprocesamiento import menciones, emoticonos, numeros, lematizacion


## Sobreescribe la misma columna
datos['Tweet'] = datos['Tweet'].apply(menciones)
datos['Tweet'] = datos['Tweet'].apply(emoticonos)
datos['Tweet'] = datos['Tweet'].apply(numeros)
datos['Tweet'] = datos['Tweet'].apply(lematizacion)


print(datos.head())

import matplotlib.pyplot as plt  

from sklearn.metrics import accuracy_score
from sklearn.metrics import plot_confusion_matrix

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.svm import OneClassSVM

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer


X_train, X_test, y_train, y_test = train_test_split( datos['Tweet'], datos['anger'], random_state=0)


## Bolsa de palabras (unigramas)
#vectorizer = CountVectorizer()

## Bolsa de palabras (bigramas)
vectorizer = CountVectorizer(ngram_range=(2, 2))

## Bolsa de palabras (TF-IDF)
#vectorizer = TfidfVectorizer(ngram_range=(2, 2))
#vectorizer = TfidfVectorizer()

## Voy creando el modelo desde acá
X_train_vec = vectorizer.fit_transform(X_train)
X_test_vec = vectorizer.transform(X_test)

print(vectorizer.get_feature_names())

# print(pd.DataFrame(X_train_vec.A, columns=vectorizer.get_feature_names()).head().to_string())

## Kernel lineal cross validation
"""
clf = SVC(random_state=0)

clf.fit(X_train_vec, y_train)

y_test_pred = clf.predict(X_test_vec)

print(accuracy_score(y_test, y_test_pred))

plot_confusion_matrix(clf, X_test_vec, y_test)  
plt.show()
"""

## Con Cross validation

# Set the parameters by cross-validation
"""
import numpy as np

tuned_parameters = [{'kernel': ['rbf'], 'gamma': np.logspace(0, 1, num=10, base =0.001, dtype=float), 'C': np.logspace(0, 10, num=5, base =1.6, dtype=int)},
                    {'kernel': ['linear'], 'C': np.logspace(0, 10, num=5, base =1.6, dtype=int)}]

clf = GridSearchCV( SVC(), tuned_parameters, cv=10, verbose=3, n_jobs=6) 
"""

tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]


clf = GridSearchCV( SVC(), tuned_parameters, cv=10, verbose=3) 

clf.fit(X_train_vec, y_train)
print(clf.best_params_)  

y_test_pred = clf.predict(X_test_vec)

print(accuracy_score(y_test, y_test_pred))


from sklearn.metrics import classification_report
target_names = [ 'No anger', 'Anger']

report = classification_report(y_test, y_test_pred, target_names=target_names)
print(report)


from sklearn.metrics import f1_score

f1 = f1_score(y_test, y_test_pred, average='macro')
print(f1)


plot_confusion_matrix(clf, X_test_vec, y_test)  
plt.show()

# OneClassSVM (Detección de outliers)
"""
tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4]}, {'kernel': ['linear']}]
clf = GridSearchCV( OneClassSVM(), tuned_parameters, cv=10, verbose=3, scoring='accuracy') 

clf.fit(X_train_vec, y_train)
print(clf.best_params_)  

y_test_pred = clf.predict(X_test_vec)

print(accuracy_score(y_test, y_test_pred))

print(y_test.to_list()[0:30])
print(list(y_test_pred)[0:30])
"""

