

import pandas as pd
import re



# Importa los datos
datos = pd.read_csv('tass_2020.tsv', sep='\t', encoding='utf-8');


## Muestra algunos datos

## Nombres de las columnas 
print(datos.columns)

## Imprime las 5 primeras líneas de la tabla
print(datos.head())

## Imprime los cinco primeros valores de la columna Tweet
print( datos['tweet'][0:5])

# Preprocesamiento

print(datos.head())

## aplica una función de preprocesamiento
from preprocesamiento import menciones


## Sobreescribe la misma columna
datos['tweet'] = datos['tweet'].apply(menciones)


import matplotlib.pyplot as plt  

from sklearn.metrics import accuracy_score
from sklearn.metrics import plot_confusion_matrix

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.svm import OneClassSVM

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer


X_train, X_test, y_train, y_test = train_test_split( datos['tweet'], datos['label'], random_state=0)


## Bolsa de palabras (unigramas)
#vectorizer = CountVectorizer()

## Bolsa de palabras (bigramas)
# vectorizer = CountVectorizer(ngram_range=(1, 2))

## Bolsa de palabras (TF-IDF)
vectorizer = TfidfVectorizer()

## Voy creando el modelo desde acá
X_train_vec = vectorizer.fit_transform(X_train)
X_test_vec = vectorizer.transform(X_test)


## Kernel lineal cross validation
clf = SVC(random_state=0)

clf.fit(X_train_vec, y_train)

y_test_pred = clf.predict(X_test_vec)

print(accuracy_score(y_test, y_test_pred))

plot_confusion_matrix(clf, X_test_vec, y_test)  
plt.show()

## Con Cross validation

# Set the parameters by cross-validation
"""
tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

clf = GridSearchCV( SVC(), tuned_parameters, cv=10, verbose=3) 

clf.fit(X_train_vec, y_train)
print(clf.best_params_)  

y_test_pred = clf.predict(X_test_vec)

print(accuracy_score(y_test, y_test_pred))

plot_confusion_matrix(clf, X_test_vec, y_test)  
#plt.show()
"""

print('----')
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE



colors = [float(hash(s) % 256) / 256 for s in y_train]

tsne = TSNE(random_state=0)
tsne_results = tsne.fit_transform(X_train_vec)

tsne_results=pd.DataFrame(tsne_results, columns=['tsne1', 'tsne2'])

plt.scatter(tsne_results['tsne1'], tsne_results['tsne2'], c=colors)
plt.show()
